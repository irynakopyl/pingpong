﻿using System;
using RabbitMQ.Wrapper;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("pinger started! (ctrl+c to exit)");
            Wrapper w = new Wrapper();
            w.SendMessageToQueue("ping", "pong_queue");
            while (true)
            {
                w.ListenQueue("ping_queue");
            }

        }
    }
}
