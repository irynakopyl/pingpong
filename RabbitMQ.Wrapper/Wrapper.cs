﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading;

namespace RabbitMQ.Wrapper
{
    public class Wrapper
    {
        private readonly IConnection connection;
        private readonly IModel channel;
        public Wrapper ()
        {
            connection = GetRabbitConnection();
            channel = connection.CreateModel();
        }

        public void ListenQueue(string currentQueue)
        {
            string oppositeQueue = "";
            string msg = "";
            if (currentQueue=="ping_queue")
            {
                oppositeQueue = "pong_queue";
                msg = "ping";
            }
            else if (currentQueue == "pong_queue")
            {
                oppositeQueue = "ping_queue";
                msg = "pong";
            }
            channel.ExchangeDeclare("Data", ExchangeType.Direct);
                channel.QueueDeclare(queue: currentQueue, durable: true, exclusive: false, autoDelete: false);
                channel.QueueBind(currentQueue, "Data", currentQueue);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine($" [x] Received {message} at {DateTime.Now}");
                    channel.BasicAck(ea.DeliveryTag, false);
                    Thread.Sleep(2500);
                    SendMessageToQueue(msg, oppositeQueue);
                };
                channel.BasicConsume(queue: currentQueue, autoAck: false, consumer: consumer);
        }
        public  bool SendMessageToQueue(string msg, string queue)
        {   
                channel.ExchangeDeclare("Data", ExchangeType.Direct);
                var body = Encoding.UTF8.GetBytes(msg);
                channel.BasicPublish(exchange: "Data", routingKey: queue, basicProperties: null, body: body);
                Console.WriteLine($" [x] Sent {msg} at {DateTime.Now}");
                return true;
        }   
        private IConnection GetRabbitConnection()
        {
            ConnectionFactory factory = new ConnectionFactory
            {
                UserName = "guest",
                Password = "guest",
                VirtualHost = "/",
                HostName = "localhost"
            };
            IConnection conn = factory.CreateConnection();
            return conn;
        }
       
        public void Dispose()
        {
            connection?.Dispose();
            channel.Dispose();
        }
    }
}
