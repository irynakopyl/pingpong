﻿using System;
using RabbitMQ.Wrapper;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("ponger started! (ctrl+c to exit)");
            Wrapper w = new Wrapper();
            while(true)
            {
                w.ListenQueue("pong_queue");
            }
            
        }
    }
}
